# YubiKey

To build the HTML presentation, install `pandoc` and run
```
pandoc \
    -t revealjs \
    -s yubikey.md \
    -o yubikey.html \
    -V revealjs-url=https://revealjs.com \
    -V transition=none \
    -V theme=moon
```
